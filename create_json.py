import json
 
# Data to be written
address = { 
    "doorno": "3-66",
    "streetname": "vinayaknagar",
    "area": "gachibowli",
    "landmark":"dlf",
    "city" : "hyderabad"
}
 
with open("address.json", "w") as file:
    json.dump(address, file)