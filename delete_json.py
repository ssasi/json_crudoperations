import json

file_name = 'add1.json'

def delete(data,filename="add1.json"):
    with open(filename, 'r', encoding='utf-8') as f:
        json.dump(data,f,indent=2)

with open('add1.json') as json_file: 
	data = json.load(json_file) 
	print(data)
	
	temp = data["address_details"] 
	temp.pop(data["area"]) 

delete(data)